class Array
  def sum
    inject(0.0) { |result, el| result + el }
  end

  def mean
    sum / size
  end

  def median
    sorted = sort
    len = sorted.length
    (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
  end

  def mode
    freq = inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    freq.key(freq.values.max)
  end
end

class LogAnalyser

  LOG_FILE_PATH  = 'sample.log'

  VALID_PATHS = [
      "GET /api/users/{user_id}/count_pending_messages",
      "GET /api/users/{user_id}/get_messages",
      "GET /api/users/{user_id}/get_friends_progress",
      "GET /api/users/{user_id}/get_friends_score",
      "POST /api/users/{user_id}",
      "GET /api/users/{user_id}"
    ]

  def initialize(log_file_path: nil)
    @log_file_path = log_file_path || LogAnalyser::LOG_FILE_PATH
    @report = {}
  end

  def run
    @report[:api_requests] ||= {}
    @report[:response_time] ||= []
    @report[:dynos] ||= []
    log_file=File.open(@log_file_path, "r")
    while(line = log_file.gets)
       analyse_api_request line
    end
  end

  def report
    report_api_requests
    puts nil
    report_response_times
    puts nil
    report_max_dynos
  end

  private

  def analyse_api_request(line)
    analyse_api_path line
    analyse_response_time line
    analyse_dynos line
  end

  def analyse_api_path(line)
    http_method = line.match(/(method=)\S*/).to_s.gsub('method=', '')
    path = line.match(/(path=)\S*/).to_s.gsub('path=', '').gsub(/\d+/, '{user_id}')
    http_method_with_path = "#{http_method} #{path}"
    return unless LogAnalyser::VALID_PATHS.include? http_method_with_path
    @report[:api_requests]["#{http_method_with_path}"] = @report[:api_requests]["#{http_method_with_path}"].to_i + 1
  end



  def analyse_response_time(line)
    connect_time = line.match(/(connect=)\S*/).to_s.gsub(/\D/, '').to_i
    service_time = line.match(/(service=)\S*/).to_s.gsub(/\D/, '').to_i
    @report[:response_time] << (connect_time + service_time)
  end

  def analyse_dynos(line)
    dyno = line.match(/(dyno=)\S*/).to_s.gsub('dyno=', '').to_sym
    @report[:dynos] << dyno
  end


  # report section

  def report_api_requests
    @report[:api_requests].each do |http_method_with_path, count|
        puts "#{http_method_with_path} : #{count}"
    end

    (VALID_PATHS - @report[:api_requests].keys).each do |http_method_with_path|
        puts "#{http_method_with_path} : #{0}"
    end
  end

  def report_max_dynos
    puts "max used dyno : #{@report[:dynos].mode}"
  end

  def report_response_times
    puts "mean response time : #{@report[:response_time].mean}"
    puts "median response time : #{@report[:response_time].median}"
    puts "mode response time : #{@report[:response_time].mode}"
  end
end


analyser = LogAnalyser.new
analyser.run
analyser.report
